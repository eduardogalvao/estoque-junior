/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.model;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author THOR
 */
public class Saida {

        private Integer saidaId;
        private Integer clienteId;
        private String prodNome;
        private Integer quantidade;
        private Double valorTotal;
        private Date data;
        private Time hora;

        public Saida() {
        }

        public Integer getSaidaId() {
                return saidaId;
        }

        public void setSaidaId(Integer entradaId) {
                this.saidaId = entradaId;
        }

        public Integer getClienteId() {
                return clienteId;
        }

        public void setClienteId(Integer clienteId) {
                this.clienteId = clienteId;
        }

        public String getProdNome() {
                return prodNome;
        }

        public void setProdNome(String prodNome) {
                this.prodNome = prodNome;
        }

        public Integer getQuantidade() {
                return quantidade;
        }

        public void setQuantidade(Integer quantidade) {
                this.quantidade = quantidade;
        }

        public Double getValorTotal() {
                return valorTotal;
        }

        public void setValorTotal(Double valorTotal) {
                this.valorTotal = valorTotal;
        }

        public Date getData() {
                return data;
        }

        public void setData(Date data) {
                this.data = data;
        }

        public Time getHora() {
                return hora;
        }

        public void setHora(Time hora) {
                this.hora = hora;
        }
        
}
