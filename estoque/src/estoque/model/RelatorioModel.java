/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.model;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author THOR
 */
public class RelatorioModel {

        private String movimento;
        private String produto;
        private Integer quantidade;
        private Date data;
        private Time hora;
        private String total;

        public RelatorioModel() {
        }

        public String getMovimento() {
                return movimento;
        }

        public void setMovimento(String movimento) {
                this.movimento = movimento;
        }

        public String getProduto() {
                return produto;
        }

        public void setProduto(String produto) {
                this.produto = produto;
        }

        public Integer getQuantidade() {
                return quantidade;
        }

        public void setQuantidade(Integer quantidade) {
                this.quantidade = quantidade;
        }

        public Date getData() {
                return data;
        }

        public void setData(Date data) {
                this.data = data;
        }

        public Time getHora() {
                return hora;
        }

        public void setHora(Time hora) {
                this.hora = hora;
        }

        public String getTotal() {
                return total;
        }

        public void setTotal(String total) {
                this.total = total;
        }

}
