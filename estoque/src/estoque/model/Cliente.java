/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.model;

/**
 *
 * @author THOR
 */
public class Cliente {
        
        private Integer clienteId;
        private String  nomeEmpresa;
        private String  cnpj;
        private String  senha;

        public Cliente() {
        }

        public Integer getClienteId() {
                return clienteId;
        }

        public void setClienteId(Integer clienteId) {
                this.clienteId = clienteId;
        }

        public String getNomeEmpresa() {
                return nomeEmpresa;
        }

        public void setNomeEmpresa(String nomeEmpresa) {
                this.nomeEmpresa = nomeEmpresa;
        }

        public String getCnpj() {
                return cnpj;
        }

        public void setCnpj(String cnpj) {
                this.cnpj = cnpj;
        }

        public String getSenha() {
                return senha;
        }

        public void setSenha(String senha) {
                this.senha = senha;
        }
        
        
        
}
