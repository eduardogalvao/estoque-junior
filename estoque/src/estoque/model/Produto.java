/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.model;

/**
 *
 * @author THOR
 */
public class Produto {

        private Integer produtoId;
        private Integer clienteId;
        private String prodNome;
        private Integer quantidade;
        private Double preco;

        public Produto() {
        }

        public Integer getProdutoId() {
                return produtoId;
        }

        public void setProdutoId(Integer produtoId) {
                this.produtoId = produtoId;
        }

        public Integer getClienteId() {
                return clienteId;
        }

        public void setClienteId(Integer clienteId) {
                this.clienteId = clienteId;
        }

        public String getProdNome() {
                return prodNome;
        }

        public void setProdNome(String prodNome) {
                this.prodNome = prodNome;
        }

        public Double getPreco() {
                return preco;
        }

        public void setPreco(Double preco) {
                this.preco = preco;
        }

        public Integer getQuantidade() {
                return quantidade;
        }

        public void setQuantidade(Integer quantidade) {
                this.quantidade = quantidade;
        }

}
