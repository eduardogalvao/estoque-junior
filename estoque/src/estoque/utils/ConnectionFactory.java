/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THOR
 */
public class ConnectionFactory {

        private String driverName = "com.mysql.cj.jdbc.Driver";
        private String url        = "jdbc:mysql://localhost:3306/estoquedb?useTimezone=true&serverTimezone=America/Sao_Paulo";
        private String usuario    = "root";
        private String senha      = "@Walkabcd189560";

        public Connection getConnection() {
                try {
                        Class.forName(driverName); 
                        return DriverManager
                               .getConnection(url, usuario, senha);
                } catch (SQLException e) {
                        throw new RuntimeException(e);
                } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
        }
}
