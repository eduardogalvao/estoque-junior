/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.utils;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Arrays;
import javax.swing.JFrame;

/**
 *
 * @author THOR
 */
public class ViewUtils {
        
        public void posicionarJFrameAoCentro(JFrame frame) {
                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2 - frame.getSize().height/2);
        }
        
        public String passwordToString(char[] password) {
                String senhaAux = Arrays.toString(password);
                senhaAux = senhaAux.replace("[", "");
                senhaAux = senhaAux.replace("]", "");
                senhaAux = senhaAux.replace(",", "");
                senhaAux = senhaAux.replace(" ", "");
                return senhaAux;
        }
        
}
