/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.DAO;

import estoque.model.Cliente;
import estoque.model.Entrada;
import estoque.model.Produto;
import estoque.model.Saida;
import estoque.utils.ConnectionFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author THOR
 */
public class EstoqueDAO {
        
        private ConnectionFactory connectionFactory = new ConnectionFactory();
        
        public List<Cliente> obterClientes() throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM clientes";
                List<Cliente> clientes = new ArrayList<>();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                while(rs.next()) {
                        Cliente cliente = new Cliente();
                        cliente.setClienteId(rs.getInt("ID_CLIENTE"));
                        cliente.setNomeEmpresa(rs.getString("EMPRESA"));
                        cliente.setCnpj(rs.getString("CNPJ"));
                        cliente.setSenha(rs.getString("SENHA"));
                        clientes.add(cliente);
                }
                pstmt.close();
                con.close();
                return clientes;
        }
        
        public Cliente obterClientePorNome(String nomeEmpresa) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM clientes WHERE EMPRESA='" + nomeEmpresa+ "'";
                Cliente cliente = new Cliente();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                if(rs.next()) {
                        cliente.setClienteId(rs.getInt("ID_CLIENTE"));
                        cliente.setNomeEmpresa(rs.getString("EMPRESA"));
                        cliente.setCnpj(rs.getString("CNPJ"));
                        cliente.setSenha(rs.getString("SENHA"));
                }
                pstmt.close();
                con.close();
                return cliente;
        }
        
        public void inserirNovoCliente(Cliente cliente) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "INSERT INTO clientes(EMPRESA, CNPJ, SENHA)"
                        + " VALUES (?, ?, ?)";
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setString(1, cliente.getNomeEmpresa());
                pstmt.setString(2, cliente.getCnpj());
                pstmt.setString(3, cliente.getSenha());
                pstmt.executeUpdate();           
                pstmt.close();
                con.close();
                System.out.println("Cliente " + cliente.getNomeEmpresa() + " incluído com sucesso!");
        }
        
        public Produto obterProdutoPorNome(Cliente cliente, String nomeProduto) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM produtos WHERE ID_CLIENTE=" + cliente.getClienteId() + " AND PROD_NOME='" + nomeProduto + "'";
                Produto produto = new Produto();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                if(rs.next()) {
                        produto.setClienteId(rs.getInt("ID_CLIENTE"));
                        produto.setProdutoId(rs.getInt("ID_PRODUTO"));
                        produto.setProdNome(rs.getString("PROD_NOME"));
                        produto.setQuantidade(rs.getInt("PROD_QTD"));
                        produto.setPreco(rs.getDouble("PRECO"));
                } else {
                        produto = null;
                }
                pstmt.close();
                con.close();
                return produto;
        }
        
        public List<Produto> obterProdutos(Cliente cliente) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM produtos WHERE ID_CLIENTE = " + cliente.getClienteId();
                List<Produto> produtos = new ArrayList<>();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                while(rs.next()) {
                        Produto produto = new Produto();
                        produto.setClienteId(rs.getInt("ID_CLIENTE"));
                        produto.setProdutoId(rs.getInt("ID_PRODUTO"));
                        produto.setProdNome(rs.getString("PROD_NOME"));
                        produto.setQuantidade(rs.getInt("PROD_QTD"));
                        produto.setPreco(rs.getDouble("PRECO"));
                        produtos.add(produto);
                }
                pstmt.close();
                con.close();
                return produtos;
        }
        
        public void inserirNovoProduto(Produto produto) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "INSERT INTO produtos(ID_CLIENTE, PROD_NOME, PROD_QTD, PRECO) VALUES(?, ?, ?, ?)";
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, produto.getClienteId());
                pstmt.setString(2, produto.getProdNome());
                pstmt.setInt(3, 0);
                pstmt.setDouble(4, produto.getPreco());
                pstmt.executeUpdate();           
                pstmt.close();
                con.close();
                System.out.println("Produto " + produto.getProdNome() + " incluído com sucesso!");
        }
        
        public Integer obterEstoqueProduto(Cliente cliente, String produtoNome) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT PROD_QTD FROM produtos WHERE ID_CLIENTE = " + cliente.getClienteId() 
                        + " AND PROD_NOME='" + produtoNome + "'";
                Integer quantidadeAtual = 0;
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                if(rs.next()) {
                        quantidadeAtual = rs.getInt("PROD_QTD");
                }
                pstmt.close();
                con.close();
                return quantidadeAtual;
        }
        
        public void alterarEstoqueProduto(Cliente cliente, String produtoNome, Integer novaQuantidade) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "UPDATE produtos SET PROD_QTD = " + novaQuantidade 
                        + " WHERE ID_CLIENTE = " + cliente.getClienteId() 
                        + " AND PROD_NOME = '" + produtoNome + "'";
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.executeUpdate();           
                pstmt.close();
                con.close();
        }
        
        public void inserirNovaEntrada(Cliente cliente, String produtoNome, Integer quantidadeEntrada, Double valorTotal, Date data, Time horario) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "INSERT INTO entrada(ID_CLIENTE, PROD_NOME, ENT_QTD, VLR_TOTAL, DT, HR) VALUES (?,?,?,?,?,?)";
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, cliente.getClienteId());
                pstmt.setString(2, produtoNome);
                pstmt.setInt(3, quantidadeEntrada);
                pstmt.setDouble(4, valorTotal);
                pstmt.setDate(5, data);
                pstmt.setTime(6, horario);
                pstmt.executeUpdate();           
                pstmt.close();
                con.close();
        }
        
        public void inserirNovaSaida(Cliente cliente, String produtoNome, Integer quantidadeSaida, Double valorTotal, Date data, Time horario) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "INSERT INTO saida(ID_CLIENTE, PROD_NOME, SAI_QTD, VLR_TOTAL, DT, HR) VALUES (?,?,?,?,?,?)";
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, cliente.getClienteId());
                pstmt.setString(2, produtoNome);
                pstmt.setInt(3, quantidadeSaida);
                pstmt.setDouble(4, valorTotal);
                pstmt.setDate(5, data);
                pstmt.setTime(6, horario);
                pstmt.executeUpdate();           
                pstmt.close();
                con.close();
        }
        
        public List<Entrada> obterEntradas(Cliente cliente) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM entrada WHERE ID_CLIENTE = " + cliente.getClienteId();
                List<Entrada> entradas = new ArrayList<>();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                while(rs.next()) {
                        Entrada entrada = new Entrada();
                        entrada.setEntradaId(rs.getInt("ID_ENTRADA"));
                        entrada.setClienteId(rs.getInt("ID_CLIENTE"));
                        entrada.setProdNome(rs.getString("PROD_NOME"));
                        entrada.setQuantidade(rs.getInt("ENT_QTD"));
                        entrada.setValorTotal(rs.getDouble("VLR_TOTAL"));
                        entrada.setData(rs.getDate("DT"));
                        entrada.setHora(rs.getTime("HR"));
                        entradas.add(entrada);
                }
                pstmt.close();
                con.close();
                return entradas;
        }
        
        public List<Saida> obterSaidas(Cliente cliente) throws SQLException {
                Connection con = connectionFactory.getConnection();
                String sql = "SELECT * FROM saida WHERE ID_CLIENTE = " + cliente.getClienteId();
                List<Saida> saidas = new ArrayList<>();
                PreparedStatement pstmt = con.prepareStatement(sql);
                ResultSet rs = pstmt.executeQuery();
                while(rs.next()) {
                        Saida saida = new Saida();
                        saida.setSaidaId(rs.getInt("ID_SAIDA"));
                        saida.setClienteId(rs.getInt("ID_CLIENTE"));
                        saida.setProdNome(rs.getString("PROD_NOME"));
                        saida.setQuantidade(rs.getInt("SAI_QTD"));
                        saida.setValorTotal(rs.getDouble("VLR_TOTAL"));
                        saida.setData(rs.getDate("DT"));
                        saida.setHora(rs.getTime("HR"));
                        saidas.add(saida);
                }
                pstmt.close();
                con.close();
                return saidas;
        }
}
