/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.render;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import static javax.swing.SwingConstants.CENTER;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author THOR
 */
public class CellRenderer_IO extends DefaultTableCellRenderer{
    public CellRenderer_IO() {
        super();
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
		boolean isSelected, boolean hasFocus, int row, int column) {
        this.setHorizontalAlignment(CENTER);
        if(column == 0 && String.valueOf(value).equals("ENTRADA")) {
            this.setForeground(new Color(100, 180, 100));
        }
        if(column == 0 && String.valueOf(value).equals("SAÍDA")) {
            this.setForeground(Color.red);
        }
         if(row%2 == 0) {
            this.setBackground(new Color(220, 255, 255));
        } else {
            this.setBackground(Color.white);
        }
	return super.getTableCellRendererComponent(table, value, isSelected,
			hasFocus, row, column);       
    }
}
