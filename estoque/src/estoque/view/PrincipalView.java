/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.view;

import estoque.DAO.EstoqueDAO;
import estoque.model.Cliente;
import estoque.model.Entrada;
import estoque.model.Produto;
import estoque.model.RelatorioModel;
import estoque.model.Saida;
import estoque.render.CellRenderer;
import estoque.render.CellRenderer_IO;
import estoque.utils.ViewUtils;
import java.awt.Font;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author THOR
 */
public class PrincipalView extends javax.swing.JFrame {

        private ViewUtils vUtils = new ViewUtils();
        private EstoqueDAO dao = new EstoqueDAO();
        private Cliente cliente = new Cliente();
        
        public PrincipalView() {
                initComponents();
        }

        public PrincipalView(Cliente cliente) {
                initComponents();
                iniciar(cliente);
        }

        public void iniciar(Cliente cliente) {
                this.cliente = cliente;
                vUtils.posicionarJFrameAoCentro(this);
                carregarDadosEmpresa();
                this.setAlwaysOnTop(true);
                try {
                        renderizarTabelaRelatorio();
                } catch (SQLException ex) {
                        Logger.getLogger(PrincipalView.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

        public void atualizarQtdProdutos(int novaQtd) {
                empresaQtdProdutos.setText(novaQtd + " produtos cadastrados");
        }

        public void carregarDadosEmpresa() {
                Integer qtdProdutos = 0;
                try {
                        qtdProdutos = dao.obterProdutos(cliente).size();
                } catch (SQLException ex) {
                        Logger.getLogger(PrincipalView.class.getName()).log(Level.SEVERE, null, ex);
                }
                empresaNome.setText(cliente.getNomeEmpresa());
                empresaCNPJ.setText(cliente.getCnpj());
                empresaQtdProdutos.setText(qtdProdutos.toString() + " produtos cadastrados");
        }

        public void renderizarTabelaRelatorio() throws SQLException {
                DefaultTableModel tableModel = (DefaultTableModel) tabelaRelatorio.getModel();
                while(tabelaRelatorio.getRowCount() > 0) {
                        tableModel.removeRow(0);
                }
                CellRenderer render = new CellRenderer();
                CellRenderer_IO renderIO = new CellRenderer_IO();
                List<Entrada> listaEntrada = dao.obterEntradas(this.cliente);
                List<Saida> listaSaida = dao.obterSaidas(this.cliente);
                List<RelatorioModel> listaRelatorioModel = new ArrayList<>();
                JTableHeader headerAux = tabelaRelatorio.getTableHeader();
                headerAux.setFont(new Font("Tahome", Font.BOLD, 16));

                for (Entrada entrada : listaEntrada) {
                        RelatorioModel relatorio = new RelatorioModel();
                        relatorio.setMovimento("ENTRADA");
                        relatorio.setProduto(entrada.getProdNome());
                        relatorio.setQuantidade(entrada.getQuantidade());
                        relatorio.setData(entrada.getData());
                        relatorio.setHora(entrada.getHora());
                        relatorio.setTotal("-");
                        listaRelatorioModel.add(relatorio);
                }

                for (Saida saida : listaSaida) {
                        RelatorioModel relatorio = new RelatorioModel();
                        relatorio.setMovimento("SAÍDA");
                        relatorio.setProduto(saida.getProdNome());
                        relatorio.setQuantidade(saida.getQuantidade());
                        relatorio.setData(saida.getData());
                        relatorio.setHora(saida.getHora());
                        DecimalFormat dformat = new DecimalFormat();
                        dformat.setMaximumFractionDigits(2);
                        dformat.setMinimumFractionDigits(2);
                        Double total = saida.getValorTotal();
                        String totalStr = dformat.format(total);
                        relatorio.setTotal("R$ " + totalStr);
                        listaRelatorioModel.add(relatorio);
                }

                RelatorioModel pivot = new RelatorioModel();
                RelatorioModel pivotAux = new RelatorioModel();
                int pos;
                for (int i = 0; i < listaRelatorioModel.size(); i++) {
                        pivot = listaRelatorioModel.get(i);
                        pivotAux = pivot;
                        pos = 0;
                        for (int j = i; j < listaRelatorioModel.size(); j++) {
                                if ((pivot.getData().getTime() + pivot.getHora().getTime()) < (listaRelatorioModel.get(j).getData().getTime() + listaRelatorioModel.get(j).getHora().getTime())) {
                                        if ((pivotAux.getData().getTime() + pivotAux.getHora().getTime()) < (listaRelatorioModel.get(j).getData().getTime() + listaRelatorioModel.get(j).getHora().getTime())) {
                                                pivotAux = listaRelatorioModel.get(j);
                                                pos = j;
                                        }
                                }
                        }
                        if (pos != 0) {
                                listaRelatorioModel.set(pos, pivot);
                                listaRelatorioModel.set(i, pivotAux);
                        }
                }

                for (int i = 0; i < listaRelatorioModel.size(); i++) {
                        tableModel.addRow(new Object[]{listaRelatorioModel.get(i).getMovimento(),
                                listaRelatorioModel.get(i).getProduto(),
                                String.valueOf(listaRelatorioModel.get(i).getQuantidade()),
                                listaRelatorioModel.get(i).getData().toString(),
                                listaRelatorioModel.get(i).getHora().toString(),
                                listaRelatorioModel.get(i).getTotal()});
                }

                for (int i = 0; i < tabelaRelatorio.getColumnCount(); i++) {
                        tabelaRelatorio.getColumnModel().getColumn(i).setCellRenderer(render);
                }
                
                tabelaRelatorio.getColumnModel().getColumn(0).setCellRenderer(renderIO);
        }

        /**
         * This method is called from within the constructor to initialize the
         * form. WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                janelaPrincipal = new javax.swing.JPanel();
                jPanel1 = new javax.swing.JPanel();
                empresaNome = new javax.swing.JLabel();
                empresaCNPJ = new javax.swing.JLabel();
                empresaQtdProdutos = new javax.swing.JLabel();
                jPanel2 = new javax.swing.JPanel();
                botaoEntrada = new javax.swing.JButton();
                botaoSaida = new javax.swing.JButton();
                botaoCadastrarProduto = new javax.swing.JButton();
                botaoEstoque = new javax.swing.JButton();
                jPanel3 = new javax.swing.JPanel();
                jScrollPane1 = new javax.swing.JScrollPane();
                tabelaRelatorio = new javax.swing.JTable();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                setTitle("TELA PRINCIPAL");
                setPreferredSize(new java.awt.Dimension(1600, 900));

                janelaPrincipal.setToolTipText("");

                jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("EMPRESA"));

                empresaNome.setText("EMPRESA");

                empresaCNPJ.setText("CNPJ");

                empresaQtdProdutos.setText("PRODUTOS CADASTRADOS");

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(empresaCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(empresaNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(empresaQtdProdutos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                );
                jPanel1Layout.setVerticalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(empresaNome)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(empresaCNPJ)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(empresaQtdProdutos)
                                .addGap(0, 8, Short.MAX_VALUE))
                );

                jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("OPÇÕES"));

                botaoEntrada.setText("ENTRADA");
                botaoEntrada.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoEntradaActionPerformed(evt);
                        }
                });

                botaoSaida.setText("SAÍDA");
                botaoSaida.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoSaidaActionPerformed(evt);
                        }
                });

                botaoCadastrarProduto.setText("CADASTRAR PRODUTO");
                botaoCadastrarProduto.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoCadastrarProdutoActionPerformed(evt);
                        }
                });

                botaoEstoque.setText("ESTOQUE");
                botaoEstoque.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoEstoqueActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
                jPanel2.setLayout(jPanel2Layout);
                jPanel2Layout.setHorizontalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(botaoEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(botaoSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(botaoCadastrarProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botaoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(742, Short.MAX_VALUE))
                );
                jPanel2Layout.setVerticalGroup(
                        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(botaoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(botaoEntrada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(botaoSaida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(botaoCadastrarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(16, Short.MAX_VALUE))
                );

                jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("RELATÓRIO"));

                tabelaRelatorio.setModel(new javax.swing.table.DefaultTableModel(
                        new Object [][] {

                        },
                        new String [] {
                                "ENTRADA/SAIDA", "PRODUTO", "QUANTIDADE", "DATA", "HORA", "TOTAL"
                        }
                ) {
                        boolean[] canEdit = new boolean [] {
                                false, false, false, false, false, false
                        };

                        public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                        }
                });
                jScrollPane1.setViewportView(tabelaRelatorio);
                if (tabelaRelatorio.getColumnModel().getColumnCount() > 0) {
                        tabelaRelatorio.getColumnModel().getColumn(0).setResizable(false);
                        tabelaRelatorio.getColumnModel().getColumn(1).setResizable(false);
                        tabelaRelatorio.getColumnModel().getColumn(2).setResizable(false);
                        tabelaRelatorio.getColumnModel().getColumn(3).setResizable(false);
                }

                javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
                jPanel3.setLayout(jPanel3Layout);
                jPanel3Layout.setHorizontalGroup(
                        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1)
                                .addContainerGap())
                );
                jPanel3Layout.setVerticalGroup(
                        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                                .addContainerGap())
                );

                javax.swing.GroupLayout janelaPrincipalLayout = new javax.swing.GroupLayout(janelaPrincipal);
                janelaPrincipal.setLayout(janelaPrincipalLayout);
                janelaPrincipalLayout.setHorizontalGroup(
                        janelaPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                janelaPrincipalLayout.setVerticalGroup(
                        janelaPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(janelaPrincipalLayout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(janelaPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(janelaPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

        private void botaoCadastrarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCadastrarProdutoActionPerformed
                CadastroProdutoView cpv = new CadastroProdutoView(this, this.cliente);
                cpv.setVisible(true);
        }//GEN-LAST:event_botaoCadastrarProdutoActionPerformed

        private void botaoEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoEntradaActionPerformed
                try {
                        if (dao.obterProdutos(this.cliente).size() != 0) {
                                EntradaProdutoView epv = new EntradaProdutoView(this, this.cliente);
                                epv.setVisible(true);
                        } else {
                                JOptionPane.showMessageDialog(this, "Não existe produtos cadastrados para este cliente!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE);
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(PrincipalView.class.getName()).log(Level.SEVERE, null, ex);
                }
        }//GEN-LAST:event_botaoEntradaActionPerformed

        private void botaoSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSaidaActionPerformed
                try {
                        if (dao.obterProdutos(this.cliente).size() != 0) {
                                SaidaProdutoView spv = new SaidaProdutoView(this, this.cliente);
                                spv.setVisible(true);
                        } else {
                                JOptionPane.showMessageDialog(this, "Não existe produtos cadastrados para este cliente!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE);
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(PrincipalView.class.getName()).log(Level.SEVERE, null, ex);
                }
        }//GEN-LAST:event_botaoSaidaActionPerformed

        private void botaoEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoEstoqueActionPerformed
                try {
                        if (dao.obterProdutos(this.cliente).size() != 0) {
                                EstoqueView ev = new EstoqueView(this, this.cliente);
                                ev.setVisible(true);
                        } else {
                                JOptionPane.showMessageDialog(this, "Não existe produtos cadastrados para este cliente!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE);
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(PrincipalView.class.getName()).log(Level.SEVERE, null, ex);
                }
        }//GEN-LAST:event_botaoEstoqueActionPerformed

        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(PrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(PrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(PrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(PrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new PrincipalView().setVisible(true);
                        }
                });
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton botaoCadastrarProduto;
        private javax.swing.JButton botaoEntrada;
        private javax.swing.JButton botaoEstoque;
        private javax.swing.JButton botaoSaida;
        private javax.swing.JLabel empresaCNPJ;
        private javax.swing.JLabel empresaNome;
        private javax.swing.JLabel empresaQtdProdutos;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JPanel jPanel2;
        private javax.swing.JPanel jPanel3;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JPanel janelaPrincipal;
        private javax.swing.JTable tabelaRelatorio;
        // End of variables declaration//GEN-END:variables
}
