/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.view;

import estoque.DAO.EstoqueDAO;
import estoque.model.Cliente;
import estoque.model.Produto;
import estoque.utils.ViewUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author THOR
 */
public class CadastroProdutoView extends javax.swing.JFrame {

        protected JFrame parent;
        private ViewUtils vUtils = new ViewUtils();
        private EstoqueDAO dao = new EstoqueDAO();
        private Cliente cliente = new Cliente();

        public CadastroProdutoView() {
                initComponents();
        }

        public CadastroProdutoView(JFrame parent, Cliente cliente) {
                initComponents();
                iniciar(parent, cliente);
        }

        public void iniciar(JFrame parent, Cliente cliente) {
                vUtils.posicionarJFrameAoCentro(this);
                this.cliente = cliente;
                this.parent = parent;
                this.parent.setEnabled(false);
                this.setAlwaysOnTop(true);
        }

        public void finalizar() {
                this.dispose();
                this.parent.setEnabled(true);
                this.parent.requestFocus();
        }

        public void limparCampos() {
                textNomeProduto.setText("");
                textPreco.setText("0,00");
        }

        /**
         * This method is called from within the constructor to initialize the
         * form. WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel1 = new javax.swing.JPanel();
                jLabel1 = new javax.swing.JLabel();
                textNomeProduto = new javax.swing.JTextField();
                jLabel2 = new javax.swing.JLabel();
                textPreco = new javax.swing.JFormattedTextField();
                botaoCadastrar = new javax.swing.JButton();
                jLabel3 = new javax.swing.JLabel();
                botaoVoltar = new javax.swing.JButton();
                jLabel4 = new javax.swing.JLabel();

                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                setTitle("NOVO PRODUTO");
                setResizable(false);
                addWindowListener(new java.awt.event.WindowAdapter() {
                        public void windowClosing(java.awt.event.WindowEvent evt) {
                                formWindowClosing(evt);
                        }
                });

                jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("CADASTRAR PRODUTO"));

                jLabel1.setText("NOME DO PRODUTO:");

                jLabel2.setText("PREÇO DO PRODUTO:");

                textPreco.setColumns(2);
                textPreco.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
                textPreco.setText("0,00");

                botaoCadastrar.setText("Cadastrar");
                botaoCadastrar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoCadastrarActionPerformed(evt);
                        }
                });

                botaoVoltar.setText("Voltar");
                botaoVoltar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoVoltarActionPerformed(evt);
                        }
                });

                jLabel4.setText("R$");

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(textNomeProduto)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabel3)
                                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                                                                                .addComponent(jLabel4)
                                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                .addComponent(textPreco))
                                                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                .addComponent(jLabel1)
                                                                                                .addComponent(jLabel2)))
                                                                                .addGap(12, 12, 12)))
                                                                .addGap(0, 0, Short.MAX_VALUE))))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGap(0, 319, Short.MAX_VALUE)
                                                .addComponent(botaoVoltar)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(botaoCadastrar)))
                                .addContainerGap())
                );
                jPanel1Layout.setVerticalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(textNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jLabel2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(textPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel4))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(botaoCadastrar)
                                        .addComponent(botaoVoltar))
                                .addContainerGap())
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

        private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
                finalizar();
        }//GEN-LAST:event_formWindowClosing

        private void botaoVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoVoltarActionPerformed
                finalizar();
        }//GEN-LAST:event_botaoVoltarActionPerformed

        private void botaoCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCadastrarActionPerformed
                if (!textNomeProduto.getText().equals("")
                        && !textPreco.getText().equals("")) {
                        try {
                                if (dao.obterProdutoPorNome(this.cliente, textNomeProduto.getText()) == null) {
                                        if (Double.valueOf(textPreco.getText().replace(",", ".")) > 0d) {
                                                Produto produto = new Produto();
                                                produto.setClienteId(this.cliente.getClienteId());
                                                produto.setProdNome(textNomeProduto.getText());
                                                produto.setPreco(Double.valueOf(textPreco.getText().replace(",", ".")));
                                                dao.inserirNovoProduto(produto);
                                                JOptionPane.showMessageDialog(this, "Produto " + produto.getProdNome() + " criado com sucesso!", "SUCESSO!", JOptionPane.PLAIN_MESSAGE);
                                                limparCampos();
                                                PrincipalView pv = (PrincipalView) this.parent;
                                                pv.atualizarQtdProdutos(dao.obterProdutos(this.cliente).size());
                                        } else {
                                                JOptionPane.showMessageDialog(this, "Seu produto está sem preço!", "ATENÇÃO!", JOptionPane.INFORMATION_MESSAGE);
                                        }
                                } else {
                                        JOptionPane.showMessageDialog(this, "PRODUTO JÁ EXISTE!", "ERRO!", JOptionPane.ERROR_MESSAGE);
                                }
                        } catch (SQLException ex) {
                                Logger.getLogger(CadastroProdutoView.class.getName()).log(Level.SEVERE, null, ex);
                        }

                } else {
                        JOptionPane.showMessageDialog(this, "Algum campo está vazio!", "ATENÇÃO!", JOptionPane.INFORMATION_MESSAGE);
                }
        }//GEN-LAST:event_botaoCadastrarActionPerformed

        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(CadastroProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(CadastroProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(CadastroProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(CadastroProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new CadastroProdutoView().setVisible(true);
                        }
                });
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton botaoCadastrar;
        private javax.swing.JButton botaoVoltar;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JTextField textNomeProduto;
        private javax.swing.JFormattedTextField textPreco;
        // End of variables declaration//GEN-END:variables
}
