/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque.view;

import estoque.DAO.EstoqueDAO;
import estoque.model.Cliente;
import estoque.model.Produto;
import estoque.utils.ViewUtils;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class EntradaProdutoView extends javax.swing.JFrame {

        protected JFrame parent;
        private ViewUtils vUtils = new ViewUtils();
        private EstoqueDAO dao = new EstoqueDAO();
        private Cliente cliente = new Cliente();
        
        public EntradaProdutoView() {
                initComponents();
        }
        
        public EntradaProdutoView(JFrame parent, Cliente cliente) {
                initComponents();
                iniciar(parent, cliente);
        }
        
        public void iniciar(JFrame parent, Cliente cliente) {
                vUtils.posicionarJFrameAoCentro(this);
                this.cliente = cliente;
                this.parent = parent;
                this.parent.setEnabled(false);
                this.setAlwaysOnTop(true);
                preencherComboBoxProdutos();
        }

        public void finalizar() {
                this.dispose();
                this.parent.setEnabled(true);
                this.parent.requestFocus();
        }
        
        public void preencherComboBoxProdutos() {
                List<Produto> listaProdutos = new ArrayList<>();
                try {
                        listaProdutos = dao.obterProdutos(this.cliente);
                } catch (SQLException ex) {
                        Logger.getLogger(EntradaProdutoView.class.getName()).log(Level.SEVERE, null, ex);
                }
                for(Produto produto : listaProdutos) {
                        comboBoxProduto.addItem(produto.getProdNome());
                }
        }
        
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                jPanel1 = new javax.swing.JPanel();
                jLabel1 = new javax.swing.JLabel();
                comboBoxProduto = new javax.swing.JComboBox<>();
                jLabel2 = new javax.swing.JLabel();
                textQuantidade = new javax.swing.JFormattedTextField();
                botaoConfirmar = new javax.swing.JButton();
                botaoVoltar = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                setTitle("NOVA ENTRADA");
                setResizable(false);
                addWindowListener(new java.awt.event.WindowAdapter() {
                        public void windowClosing(java.awt.event.WindowEvent evt) {
                                formWindowClosing(evt);
                        }
                });

                jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("ENTRADA DE ESTOQUE"));

                jLabel1.setText("PRODUTO:");

                comboBoxProduto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "- SELECIONE UM PRODUTO -" }));

                jLabel2.setText("QUANTIDADE:");

                textQuantidade.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));
                textQuantidade.setHorizontalAlignment(javax.swing.JTextField.CENTER);
                textQuantidade.setText("1");

                botaoConfirmar.setText("CONFIRMAR");
                botaoConfirmar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoConfirmarActionPerformed(evt);
                        }
                });

                botaoVoltar.setText("VOLTAR");
                botaoVoltar.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                botaoVoltarActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(botaoVoltar)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(botaoConfirmar))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addGap(23, 23, 23)))
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(textQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(comboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(16, Short.MAX_VALUE))
                );
                jPanel1Layout.setVerticalGroup(
                        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(comboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(textQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(botaoConfirmar)
                                        .addComponent(botaoVoltar))
                                .addContainerGap())
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
                layout.setVerticalGroup(
                        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );

                pack();
        }// </editor-fold>//GEN-END:initComponents

        private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
                finalizar();
        }//GEN-LAST:event_formWindowClosing

        private void botaoVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoVoltarActionPerformed
                finalizar();
        }//GEN-LAST:event_botaoVoltarActionPerformed

        private void botaoConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoConfirmarActionPerformed
                if(comboBoxProduto.getSelectedIndex() > 0
                        && !textQuantidade.getText().equals("")) {
                        try {
                                String produtoSelecionado = comboBoxProduto.getSelectedItem().toString();
                                Integer quantidadeEntrada = Integer.valueOf(textQuantidade.getText());
                                Integer estoqueAtual = dao.obterEstoqueProduto(this.cliente, produtoSelecionado);
                                Integer novaQuantidade = quantidadeEntrada + estoqueAtual;
                                dao.inserirNovaEntrada(this.cliente, produtoSelecionado, quantidadeEntrada, 0d, new Date(System.currentTimeMillis()), new Time(System.currentTimeMillis()));
                                dao.alterarEstoqueProduto(this.cliente, produtoSelecionado, novaQuantidade);
                                JOptionPane.showMessageDialog(this, "Entrada de produto realizada com sucesso!", "SUCESSO!", JOptionPane.PLAIN_MESSAGE);
                                finalizar();
                                PrincipalView pv = (PrincipalView) this.parent;
                                pv.renderizarTabelaRelatorio();
                        } catch (SQLException ex) {
                                Logger.getLogger(EntradaProdutoView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                } else {
                        JOptionPane.showMessageDialog(this, "Campo vazio ou produto não selecionado!", "ATENÇÃO!", JOptionPane.INFORMATION_MESSAGE);
                }
        }//GEN-LAST:event_botaoConfirmarActionPerformed

        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(EntradaProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(EntradaProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(EntradaProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(EntradaProdutoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new EntradaProdutoView().setVisible(true);
                        }
                });
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton botaoConfirmar;
        private javax.swing.JButton botaoVoltar;
        private javax.swing.JComboBox<String> comboBoxProduto;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JFormattedTextField textQuantidade;
        // End of variables declaration//GEN-END:variables
}
